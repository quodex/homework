from setuptools import setup

VERSION = '0.1.0'
setup(
    name='jsonplaceholder_client',
    version=VERSION,
    install_requires=[
        'allure-pytest==2.8.12',
        'allure-python-commons==2.8.12',
        'Faker==4.0.2',
        'PyHamcrest==2.0.2',
        'pytest==5.4.1',
        'requests==2.23.0'
    ],
    description='OZON homework',
    packages=['framework'],
    author='Anton Sinyaev',
    author_email='belomorovich@gmail.com',

    entry_points={'pytest11': ['api_client = framework.pytest_plugin']},
    classifiers=['Framework :: Pytest'],
)
