import pytest
from faker import Faker

from framework.framework.helper import generate_post_via_random_user_id, generate_post_no_user_id
from framework.framework.jsonplaceholder_client import TransportClient, DTOClient


@pytest.fixture(scope="session")
def transport_client():
    return TransportClient()


@pytest.fixture(scope="session")
def dto_client():
    return DTOClient()


@pytest.fixture(scope="session")
def post_with_user_id():
    return generate_post_via_random_user_id()


@pytest.fixture(scope="session")
def post_without_user_id():
    return generate_post_no_user_id()


@pytest.fixture(scope="session")
def valid_post_id(transport_client):
    posts_total = len(transport_client.get_all_posts().json())
    return Faker().pyint(min_value=1, max_value=posts_total)


@pytest.fixture(scope="session")
def invalid_post_id(transport_client):
    posts_total = len(transport_client.get_all_posts().json())
    return Faker().pyint(min_value=posts_total, max_value=posts_total * 2)


@pytest.fixture(scope="session")
def post_with_certain_lang_and_user_id(request):
    return generate_post_via_random_user_id(Faker(locale=request))


@pytest.fixture(scope="session")
def all_users(transport_client):
    return transport_client.get_all_users()


@pytest.fixture(scope="session")
def invalid_user_id(all_users):
    users_count = len(all_users.json())
    return Faker().pyint(min_value=users_count, max_value=users_count * 2)
