from faker import Faker

from framework.framework.data_model.data_model import Post


def generate_post_no_user_id(faker=Faker()) -> Post:
    return Post(
        title=faker.name(),
        body=faker.text()
    )


def generate_post_via_random_user_id(faker=Faker()) -> Post:
    result = generate_post_no_user_id(faker)
    result.userId = str(faker.pyint(min_value=0, max_value=100))
    return result
