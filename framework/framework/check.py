import allure
from framework.data_model.data_model import Post
from hamcrest import assert_that, equal_to
from requests import codes


@allure.step("Check response code of response")
def check_status_code(response, expected_code):
    assert_that(response.status_code, equal_to(expected_code),
                f'Expected status code: {expected_code}. Actual code: {response.status_code}. Url: {response.url}')


@allure.step("Check response code and length of returned list")
def check_response_len(response, expected_len):
    assert_that(len(response.json()), equal_to(expected_len))


@allure.step("Check returned posts list has expected length")
def check_posts_list_len(posts, expected_len):
    actual_len = len(posts)
    assert_that(actual_len, equal_to(expected_len),
                f'Expected list length: {expected_len}. Actual list length: {actual_len}.')


@allure.step("Check all posts has same expected userId")
def check_post_has_same_expected_user_id(actual_post, expected_id):
    assert_that(str(actual_post.userId), equal_to(str(expected_id)),
                f'Expected userId for post id={actual_post.id}: {expected_id}. '
                f'Actual userId for post id={actual_post.id}: {actual_post.userId}.')


@allure.step("Check body of post is same as expected")
def check_post_contains_expected_content(actual_post: Post, expected_post: Post):
    assert_that(str(actual_post.userId), equal_to(str(expected_post.userId)),
                f"Expected userId: {expected_post.userId}. Actual userId: {actual_post.userId}")
    assert_that(actual_post.title, equal_to(expected_post.title),
                f"Expected title: {expected_post.title}. Actual title: {actual_post.title}")
    assert_that(actual_post.body, equal_to(expected_post.body),
                f"Expected body: {expected_post.body}. Actual body: {actual_post.body}")


@allure.step("Check response Id")
def check_response_contains_expected_id(actual_post: Post, expected_id):
    assert_that(actual_post.id, equal_to(expected_id), f"Expected Id: {expected_id}. Actual Id: {actual_post.id}")
