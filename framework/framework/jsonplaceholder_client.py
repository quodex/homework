import json
import logging

import allure
import requests as r
from framework.framework.data_model.data_model import Post

from config import JSONPLACEHOLDER_HOST

LOGGER = logging.getLogger(__name__)


# Session level
class _SessionClient:

    @allure.step
    def get(self, path: str):
        return r.get(url=JSONPLACEHOLDER_HOST + path)

    @allure.step
    def post(self, path: str, body: dict):
        return r.post(url=JSONPLACEHOLDER_HOST + path, data=body)

    @allure.step
    def delete(self, path: str):
        return r.delete(url=JSONPLACEHOLDER_HOST + path)

    @allure.step
    def patch(self, path: str, body: dict):
        return r.patch(url=JSONPLACEHOLDER_HOST + path, data=body)

    @allure.step
    def put(self, path: str, body: dict):
        return r.put(url=JSONPLACEHOLDER_HOST + path, data=body)


# Transport level
class TransportClient:
    _session = _SessionClient()

    # posts commands
    @allure.step("Get all posts")
    def get_all_posts(self):
        LOGGER.info("Try to build and send GET request to get all posts...")
        response = self._session.get(path='/posts')
        LOGGER.info("Request sent successfully")
        return response

    @allure.step("Get post by Id")
    def get_post_by_id(self, post_id: int):
        LOGGER.info(f"Try to build and send GET request to get post by id={post_id}...")
        response = self._session.get(path=f'/posts/{post_id}')
        LOGGER.info("Request sent successfully")
        return response

    @allure.step("Get list of posts by userId")
    def get_posts_list_by_user_id(self, user_id: str):
        LOGGER.info(f"Try to build and send GET request to get posts by userId={user_id}...")
        response = self._session.get(path=f'/posts?userId={user_id}')
        LOGGER.info("Request sent successfully")
        return response

    @allure.step("Delete post by Id")
    def delete_post_by_id(self, post_id: int):
        LOGGER.info(f"Try to build and send DELETE request to get post by id={post_id}...")
        response = self._session.delete(path=f'/posts/{post_id}')
        LOGGER.info("Request sent successfully")
        return response

    @allure.step("Create post")
    def post_post(self, body: Post):
        LOGGER.info(f"Try to build and send POST request to post the post...")
        LOGGER.info(f"Post body:\n {json.dumps(body.__dict__, indent=4)}\n")
        response = self._session.post(path='/posts', body=body.__dict__)
        LOGGER.info("Request sent successfully")
        return response

    @allure.step("Patch post")
    def patch_post_by_id(self, post_id: int, body: Post):
        LOGGER.info(f"Try to build and send PATCH request to update the post by id={post_id}...")
        LOGGER.info(f"Post body:\n {json.dumps(body.__dict__, indent=4)}\n")
        response = self._session.patch(path=f'/posts/{post_id}', body=body.__dict__)
        LOGGER.info("Request sent successfully")
        return response

    @allure.step("Put post by Id")
    def put_post_by_id(self, post_id: int, body: Post):
        LOGGER.info(f"Try to build and send PUT request to update the post by id={post_id}...")
        LOGGER.info(f"Post body:\n {json.dumps(body.__dict__, indent=4)}\n")
        response = self._session.put(path=f'/posts/{post_id}', body=body.__dict__)
        LOGGER.info("Request sent successfully")
        return response

    # users commands
    @allure.step("Get all users")
    def get_all_users(self):
        LOGGER.info("Try to build and send GET request to get all users list...")
        response = self._session.get(path='/users')
        LOGGER.info("Request sent successfully")
        return response


# DTO wrapper
class DTOClient:
    _transport = TransportClient()

    @allure.step("Get post from GET by id response and wrap it with DTO")
    def get_post_by_id(self, post_id):
        response = self._transport.get_post_by_id(post_id)
        response.raise_for_status()
        return Post(**response.json())

    @allure.step("Get all posts from GET response and wrap them with DTO")
    def get_posts_by_user_id(self, user_id: str):
        response = self._transport.get_posts_list_by_user_id(user_id)
        response.raise_for_status()
        return [Post(**post) for post in response.json()]

    @allure.step("Get post from POST response and wrap it with DTO")
    def post_post(self, body: Post):
        response = self._transport.post_post(body)
        response.raise_for_status()
        return Post(**response.json())

    @allure.step("Get post from PATCH by id response and wrap it with DTO")
    def patch_post_by_id(self, post_id: int, body: Post):
        response = self._transport.patch_post_by_id(post_id=post_id, body=body)
        response.raise_for_status()
        return Post(**response.json())

    @allure.step("Get post from PUT by id response and wrap it with DTO")
    def put_post_by_id(self, post_id: int, body: Post):
        response = self._transport.put_post_by_id(post_id=post_id, body=body)
        response.raise_for_status()
        return Post(**response.json())
