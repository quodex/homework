import allure
import pytest
from framework.check import check_post_contains_expected_content, check_response_len, \
    check_posts_list_len, check_response_contains_expected_id, \
    check_post_has_same_expected_user_id
from framework.jsonplaceholder_client import DTOClient

from resources.test_data import EXPECTED_POST


@allure.suite("GET /posts content tests")
class TestGetPosts:
    EXPECTED_USER_ID = '1'

    @allure.title('Positive. Get post with certain id and check content')
    def test_content_get_post_by_valid_id(self, dto_client):
        with allure.step("Get existed post and make sure all fields returned as expected"):
            post_from_api = dto_client.get_post_by_id(EXPECTED_POST.id)
            check_response_contains_expected_id(actual_post=post_from_api, expected_id=EXPECTED_POST.id)
            check_post_contains_expected_content(actual_post=post_from_api, expected_post=EXPECTED_POST)

    @allure.title('Positive. Get all posts')
    def test_get_all_posts(self, transport_client):
        with allure.step("Get all posts and check response"):
            response = transport_client.get_all_posts()
            check_response_len(response=response, expected_len=100)

    @allure.title('Positive. Get posts by valid user id')
    def test_get_posts_list_by_valid_user_id(self, dto_client):
        with allure.step("Get posts list by user id as argument. Make sure that answer has expected len"):
            expected_len = 10
            posts_list = dto_client.get_posts_by_user_id(self.EXPECTED_USER_ID)
            check_posts_list_len(posts=posts_list, expected_len=expected_len)

    @allure.title('Positive. Get posts by valid user id')
    @pytest.mark.parametrize("post_by_user_id", DTOClient().get_posts_by_user_id(EXPECTED_USER_ID))
    def test_get_posts_list_by_valid_user_id(self, post_by_user_id):
        with allure.step("Get posts list by user id as argument. Make sure that answer has expected len"):
            check_post_has_same_expected_user_id(actual_post=post_by_user_id, expected_id=self.EXPECTED_USER_ID)

    @allure.title('Negative. Get tests_posts by invalid user id as argument')
    def test_get_post_by_invalid_user_id(self, dto_client, invalid_user_id):
        with allure.step("Get posts list by invalid user id as argument. Make sure that nothing returned"):
            posts_list = dto_client.get_posts_by_user_id(invalid_user_id)
            check_posts_list_len(posts=posts_list, expected_len=0)

    @allure.title('Negative. Get tests_posts by empty user id as argument')
    def test_get_post_by_empty_user_id(self, dto_client):
        with allure.step("Get posts list by empty user id as argument. Make sure that nothing returned"):
            posts_list = dto_client.get_posts_by_user_id("")
            check_posts_list_len(posts=posts_list, expected_len=0)

    @allure.title('Negative. Get tests_posts by simple SQL injection')
    @pytest.mark.parametrize("simple_injection_argument", ["--", "/*", " OR 1=1", " OR 1=1 --"])
    def test_get_post_by_simple_injection(self, dto_client, simple_injection_argument):
        with allure.step("Try to make primitive SQL injection. Make sure nothing returned in response body"):
            posts_list = dto_client.get_posts_by_user_id(simple_injection_argument)
            check_posts_list_len(posts=posts_list, expected_len=0)
