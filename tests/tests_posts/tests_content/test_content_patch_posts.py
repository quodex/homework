from copy import deepcopy

import allure
import pytest
from framework.check import check_post_contains_expected_content

from resources.test_data import EXPECTED_POST, LOCALES


@allure.suite('PATCH /posts content tests')
class TestsPatchPosts:

    @allure.title("Positive. Check PATCH response contains update post info")
    def test_patch_existed_post(self, dto_client):
        with allure.step("Try to patch existed post. Make sure response contains expected content"):
            new_post_data = deepcopy(EXPECTED_POST)
            new_post_data.title = "This title updated by test"
            new_post_data.body = "This body updated by test"
            post_patch_response = dto_client.patch_post_by_id(EXPECTED_POST.id, new_post_data)
            check_post_contains_expected_content(actual_post=post_patch_response, expected_post=new_post_data)

    @allure.title("Positive. Check PATCH really updates post.")
    def test_patch_existed_post(self, dto_client):
        with allure.step("Try to patch existed post."):
            new_post_data = deepcopy(EXPECTED_POST)
            new_post_data.title = "This title updated by test"
            new_post_data.body = "This body updated by test"
            dto_client.patch_post_by_id(EXPECTED_POST.id, new_post_data)

        with allure.step("Make sure post really updated"):
            updated_post = dto_client.get_post_by_id(EXPECTED_POST.id)
            check_post_contains_expected_content(actual_post=updated_post, expected_post=new_post_data)

    @allure.title('Positive. PATCH the post with different languages, and make sure response contain expected content')
    @pytest.mark.parametrize('post_with_certain_lang_and_user_id', LOCALES, indirect=True)
    def test_patch_response_at_different_languages(self, dto_client, post_with_certain_lang_and_user_id):
        with allure.step("Update post by id with specific language text."):
            put_response_post = dto_client.patch_post_by_id(EXPECTED_POST.id, post_with_certain_lang_and_user_id)
            check_post_contains_expected_content(actual_post=put_response_post,
                                                 expected_post=post_with_certain_lang_and_user_id)

    @allure.title('Positive. PATCH the post with different languages, and make sure post really updated')
    @pytest.mark.parametrize('post_with_certain_lang_and_user_id', LOCALES, indirect=True)
    def test_patch_really_updates_post_at_different_languages(self, dto_client, post_with_certain_lang_and_user_id):
        with allure.step("Update post by id with specific language text."):
            dto_client.patch_post_by_id(EXPECTED_POST.id, post_with_certain_lang_and_user_id)

        with allure.step("Make sure that post really updated by getting post by id"):
            updated_post = dto_client.get_post_by_id(EXPECTED_POST.id)
            check_post_contains_expected_content(actual_post=updated_post,
                                                 expected_post=post_with_certain_lang_and_user_id)
