import allure
import pytest
from framework.check import check_post_contains_expected_content

from resources.test_data import LOCALES


@allure.suite('POST /posts content tests')
class TestPostPosts:

    @allure.title('Positive. Post the post and check post response contains data of request')
    def test_response_of_post(self, dto_client, post_with_user_id):
        with allure.step("Create post at backend."):
            created_post = dto_client.post_post(post_with_user_id)
            check_post_contains_expected_content(actual_post=created_post, expected_post=post_with_user_id)

    @allure.title('Positive. Post the post and check post really created')
    def test_post_really_created(self, dto_client, post_with_user_id):
        with allure.step("Create post at backend."):
            created_post = dto_client.post_post(post_with_user_id)

        with allure.step("Make sure that post really created by requesting it by Id"):
            new_post = dto_client.get_post_by_id(created_post.id)
            check_post_contains_expected_content(actual_post=new_post, expected_post=post_with_user_id)

    @allure.title('Positive. Post the post with different languages and check that response contains data of request')
    @pytest.mark.parametrize('post_with_certain_lang_and_user_id', LOCALES, indirect=True)
    def test_response_of_post_at_different_languages(self, dto_client, post_with_certain_lang_and_user_id):
        with allure.step("Create post at backend and make sure that answer is 201 and contains expected data"):
            created_post = dto_client.post_post(post_with_certain_lang_and_user_id)
            check_post_contains_expected_content(actual_post=created_post,
                                                 expected_post=post_with_certain_lang_and_user_id)

    @allure.title('Positive. Post the tests_posts with different languages and check post really created')
    @pytest.mark.parametrize('post_with_certain_lang_and_user_id', LOCALES, indirect=True)
    def test_post_really_created_at_different_languages(self, dto_client, post_with_certain_lang_and_user_id):
        with allure.step("Create post at backend and make sure that answer is 201 and contains expected data"):
            created_post = dto_client.post_post(post_with_certain_lang_and_user_id)

        with allure.step("Make sure that post really created by requesting it by Id"):
            new_post = dto_client.get_post_by_id(created_post.id)
            check_post_contains_expected_content(actual_post=new_post,
                                                 expected_post=post_with_certain_lang_and_user_id)
