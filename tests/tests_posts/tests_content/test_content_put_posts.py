from copy import deepcopy

import allure
import pytest
from framework.check import check_post_contains_expected_content

from resources.test_data import EXPECTED_POST, LOCALES


@allure.suite("PUT /posts content tests")
class TestsPutPosts:

    @allure.title("Positive. Check put updates posts")
    def test_put_response_contains_updated_data(self, dto_client):
        with allure.step("Update post by Id. Make sure response contains updated content."):
            new_post_data = deepcopy(EXPECTED_POST)
            new_post_data.title = "This title updated by test"
            new_post_data.body = "This body updated by test"
            updated_post = dto_client.put_post_by_id(EXPECTED_POST.id, new_post_data)
            check_post_contains_expected_content(actual_post=updated_post, expected_post=new_post_data)

    @allure.title("Positive. Check put updates posts")
    def test_put_updated_post_contains_new_data_after_update(self, dto_client):
        with allure.step("Update post by Id."):
            new_post_data = deepcopy(EXPECTED_POST)
            new_post_data.title = "This title updated by test"
            new_post_data.body = "This body updated by test"
            dto_client.put_post_by_id(EXPECTED_POST.id, new_post_data)

        with allure.step("Get updated post. Make sure it contains expected content by getting post by id"):
            updated_post = dto_client.get_post_by_id(EXPECTED_POST.id)
            check_post_contains_expected_content(actual_post=updated_post, expected_post=new_post_data)

    @allure.title("Positive. Check put return update data if id to update not exist")
    def test_put_not_existed_post_response_contain_update_data(self, dto_client, invalid_post_id, post_with_user_id):
        with allure.step("Try to put not existed post. Make sure response contains updated data"):
            post_with_user_id.id = invalid_post_id
            post_from_response = dto_client.put_post_by_id(EXPECTED_POST.id, post_with_user_id)
            check_post_contains_expected_content(actual_post=post_from_response, expected_post=post_with_user_id)

    @allure.title("Positive. Check put will create new post if id to update not exist")
    def test_put_not_existed_post_will_create_new_post(self, dto_client, invalid_post_id, post_with_user_id):
        with allure.step("Try to put not existed post"):
            post_with_user_id.id = invalid_post_id
            dto_client.put_post_by_id(EXPECTED_POST.id, post_with_user_id)

        with allure.step("Get new post. Make sure that post really created by getting post by id"):
            new_post = dto_client.get_post_by_id(invalid_post_id)
            check_post_contains_expected_content(actual_post=new_post, expected_post=post_with_user_id)

    @allure.title('Positive. Put the post with different languages, and make sure response contain expected content')
    @pytest.mark.parametrize('post_with_certain_lang_and_user_id', LOCALES, indirect=True)
    def test_put_response_at_different_languages(self, dto_client, post_with_certain_lang_and_user_id):
        with allure.step("Update post by id with specific language text."):
            put_response_post = dto_client.put_post_by_id(EXPECTED_POST.id, post_with_certain_lang_and_user_id)
            check_post_contains_expected_content(actual_post=put_response_post,
                                                 expected_post=post_with_certain_lang_and_user_id)

    @allure.title('Positive. Put the post with different languages, and make sure post really updated')
    @pytest.mark.parametrize('post_with_certain_lang_and_user_id', LOCALES, indirect=True)
    def test_put_really_updates_post_at_different_languages(self, dto_client, post_with_certain_lang_and_user_id):
        with allure.step("Update post by id with specific language text."):
            dto_client.put_post_by_id(EXPECTED_POST.id, post_with_certain_lang_and_user_id)

        with allure.step("Make sure that post really updated by getting post by id"):
            updated_post = dto_client.get_post_by_id(EXPECTED_POST.id)
            check_post_contains_expected_content(actual_post=updated_post,
                                                 expected_post=post_with_certain_lang_and_user_id)
