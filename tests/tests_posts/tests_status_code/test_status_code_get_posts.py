import allure
from framework.check import check_status_code
from requests import codes

from resources.test_data import EXPECTED_POST


@allure.suite("GET /posts status code tests")
class TestGetPosts:

    @allure.title('Positive. Get post with certain id and check status code. Expected code = 200')
    def test_get_post_by_valid_id_status_code(self, transport_client):
        with allure.step("Get existed post. Make sure status code = 200"):
            response = transport_client.get_post_by_id(EXPECTED_POST.id)
            check_status_code(response=response, expected_code=codes.ok)

    @allure.title('Negative. Get post with invalid id. Expected code = 404')
    def test_get_post_by_invalid_id_status_code(self, transport_client, invalid_post_id):
        with allure.step("Try to get post by invalid_id. Make sure status code = 404"):
            response = transport_client.get_post_by_id(invalid_post_id)
            check_status_code(response=response, expected_code=codes.not_found)

    @allure.title('Positive. Get all posts. Expected code = 200')
    def test_get_all_posts_status_code(self, transport_client):
        with allure.step("Get all posts. Make sure that status code = 200"):
            response = transport_client.get_all_posts()
            check_status_code(response=response, expected_code=codes.ok)

    @allure.title('Positive. Get posts by valid user id. Expected code = 200')
    def test_get_posts_list_by_valid_user_id_status_code(self, transport_client):
        with allure.step("Get posts list by user id as argument. Make sure status code = 200"):
            expected_id = "1"
            response = transport_client.get_posts_list_by_user_id(expected_id)
            check_status_code(response=response, expected_code=codes.ok)

    @allure.title('Negative. Get tests_posts by invalid user id as argument. Expected code = 200')
    def test_get_post_by_invalid_user_id_status_code(self, transport_client, invalid_user_id):
        with allure.step("Get posts list by invalid user id as argument. Make sure status code = 200"):
            response = transport_client.get_posts_list_by_user_id(invalid_user_id)
            check_status_code(response=response, expected_code=codes.ok)

    @allure.title('Negative. Get tests_posts by empty user id as argument. Expected code = 200')
    def test_get_post_by_empty_user_id_status_code(self, transport_client):
        with allure.step("Get posts list by empty user id as argument. Make sure status code = 200"):
            response = transport_client.get_posts_list_by_user_id("")
            check_status_code(response=response, expected_code=codes.ok)
