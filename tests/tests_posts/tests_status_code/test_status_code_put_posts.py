from copy import deepcopy

import allure
import pytest
from framework.check import check_status_code
from requests import codes

from resources.test_data import EXPECTED_POST, LOCALES


@allure.suite("PUT /posts status code tests")
class TestsPutPosts:

    @allure.title("Positive. Check put updates posts. Expected code = 200")
    def test_put_existed_post_status_code(self, transport_client):
        with allure.step("Try to patch existed post. Make sure status code = 200"):
            new_post_data = deepcopy(EXPECTED_POST)
            new_post_data.title = "This title updated by test"
            new_post_data.body = "This body updated by test"
            response = transport_client.put_post_by_id(EXPECTED_POST.id, new_post_data)
            check_status_code(response=response, expected_code=codes.ok)

    @allure.title("Positive. Check put updates existed post by invalid userId. Expected code = 500")
    def test_put_existed_post_by_invalid_user_id_status_code(self, transport_client, invalid_user_id):
        with allure.step("Try to patch existed post with invalid user id. Make sure status code = 500"):
            new_post_data = deepcopy(EXPECTED_POST)
            new_post_data.userId = invalid_user_id
            response = transport_client.put_post_by_id(EXPECTED_POST.id, new_post_data)
            check_status_code(response=response, expected_code=codes.internal_server_error)

    @allure.title("Positive. Check put updates existed post by invalid userId. Expected code = 500")
    def test_put_existed_post_by_invalid_user_id_status_code(self, transport_client, invalid_post_id):
        with allure.step("Try to patch existed post with invalid user id. Make sure status code = 500"):
            new_post_data = deepcopy(EXPECTED_POST)
            new_post_data.id = invalid_post_id
            response = transport_client.put_post_by_id(invalid_post_id, new_post_data)
            check_status_code(response=response, expected_code=codes.internal_server_error)

    @allure.title("Positive. Check put will create new post if try to updated by not existed Id. Expected code = 201")
    def test_put_not_existed_post_status_code(self, transport_client, invalid_post_id, post_with_user_id):
        with allure.step("Try to put not existed post. Make sure status code = 201"):
            post_with_user_id.id = invalid_post_id
            response = transport_client.put_post_by_id(post_with_user_id.id, post_with_user_id)
            check_status_code(response=response, expected_code=codes.created)

    @allure.title('Positive. Put the post with different languages. Expected code = 200')
    @pytest.mark.parametrize('post_with_certain_lang_and_user_id', LOCALES, indirect=True)
    def test_put_different_languages_status_code(self, transport_client, post_with_certain_lang_and_user_id):
        with allure.step("Update post at backend. Make sure status code = 200"):
            response = transport_client.put_post_by_id(EXPECTED_POST.id, post_with_certain_lang_and_user_id)
            check_status_code(response=response, expected_code=codes.ok)
