import allure
from framework.check import check_status_code
from requests import codes


@allure.suite('DEL /posts status code tests')
class TestDelPosts:

    @allure.title("Positive. Delete the post by valid id. Expected code = 200")
    def test_delete_post_valid_id_status_code(self, transport_client, valid_post_id):
        with allure.step("Delete post from backend. Make sure status code = 200"):
            response = transport_client.delete_post_by_id(valid_post_id)
            check_status_code(response=response, expected_code=codes.ok)

    @allure.title("Positive. Delete the post by valid id and try get it after delete. Expected code = 404")
    def test_get_status_code_deleted_post_status_code(self, transport_client, valid_post_id):
        with allure.step("Delete post from backend."):
            transport_client.delete_post_by_id(valid_post_id)

        with allure.step("Try to get deleted post from backend. Make sure status code = 404"):
            deleted_post_response = transport_client.get_post_by_id(valid_post_id)
            check_status_code(response=deleted_post_response, expected_code=codes.not_found)

    @allure.title("Negative. Delete the post by invalid id. Expected code = 404")
    def test_delete_status_code_post_invalid_id_status_code(self, transport_client, invalid_post_id):
        with allure.step("Try to delete post by invalid id. Make sure status code = 404"):
            response = transport_client.delete_post_by_id(invalid_post_id)
            check_status_code(response=response, expected_code=codes.not_found)

    @allure.title("Negative. Delete the post with empty id. Expected code = 404")
    def test_delete_post_empty_id_status_code(self, transport_client):
        with allure.step("Try to delete post by empty id. Make sure status code = 404"):
            response = transport_client.delete_post_by_id("")
            check_status_code(response=response, expected_code=codes.not_found)
