import allure
import pytest
from framework.check import check_status_code
from framework.data_model.data_model import Post
from requests import codes

from resources.test_data import LOCALES


@allure.suite("POST /posts status code tests")
class TestPostPosts:
    @allure.title('Positive. Post the post. Expected code = 201')
    def test_post_post_status_code(self, transport_client, post_with_user_id):
        with allure.step("Create post at backend. Make sure status code = 201"):
            response = transport_client.post_post(post_with_user_id)
            check_status_code(response=response, expected_code=codes.created)

    @allure.title('Positive. Post the tests_posts with different languages. Expected code = 201')
    @pytest.mark.parametrize('post_with_certain_lang_and_user_id', LOCALES, indirect=True)
    def test_post_post_at_different_languages_status_code(self, transport_client, post_with_certain_lang_and_user_id):
        with allure.step("Create post at backend and make sure status code = 201"):
            response = transport_client.post_post(post_with_certain_lang_and_user_id)
            check_status_code(response=response, expected_code=codes.created)

    @allure.title('Negative. Post the post with existed id. Expected code = 500')
    def test_post_post_with_existed_id_status_code(self, transport_client, post_with_user_id):
        # Not sure about error code
        with allure.step("Try to Create post at backend with existed Id. Make sure status code = 500"):
            post_with_user_id.id = 1
            response = transport_client.post_post(post_with_user_id)
            check_status_code(response=response, expected_code=codes.internal_server_error)

    @allure.title('Negative. Post the post with empty text and title. Expected code = 500')
    def test_post_post_with_empty_text_and_title_status_code(self, transport_client):
        # Not sure about error code
        with allure.step("Try to Create post at backend with empty title and body. Make sure status code = 500"):
            post = Post(body="", title="")
            response = transport_client.post_post(post)
            check_status_code(response=response, expected_code=codes.internal_server_error)

    @allure.title('Negative. Post the post with invalid user id. Expected code = 500')
    def test_post_post_with_invalid_user_id_status_code(self, transport_client, post_without_user_id, invalid_user_id):
        # Not sure about error code
        with allure.step("Try to Create post at backend with wrong user id. Make sure status code = 500"):
            post_without_user_id.userId = invalid_user_id
            response = transport_client.post_post(post_without_user_id)
            check_status_code(response=response, expected_code=codes.internal_server_error)

    @allure.title('Negative. Post the post with empty user id. Expected code = 500')
    def test_post_post_post_empty_user_id_status_code(self, transport_client, post_without_user_id):
        # Not sure about error code
        with allure.step("Try to Create post at backend without user id. Make sure status code = 500"):
            response = transport_client.post_post(post_without_user_id)
            check_status_code(response=response, expected_code=codes.internal_server_error)
